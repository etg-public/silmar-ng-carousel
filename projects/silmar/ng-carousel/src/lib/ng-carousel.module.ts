import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@angular/cdk/layout';
import { NgCarouselComponent } from './ng-carousel.component';
import { NgCarouselItemDirective } from './ng-carousel-item.directive';
import { NgCarouselNavigationComponent } from './ng-carousel-navigation.component';

@NgModule({
  imports      : [
    CommonModule,
    LayoutModule
  ],
  declarations : [ NgCarouselComponent, NgCarouselItemDirective, NgCarouselNavigationComponent ],
  exports      : [ NgCarouselComponent, NgCarouselItemDirective, NgCarouselNavigationComponent ]
})
export class NgCarouselModule {
}

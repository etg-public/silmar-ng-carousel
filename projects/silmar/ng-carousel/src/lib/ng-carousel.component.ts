import {
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef, EventEmitter,
  HostListener,
  Inject,
  Input,
  NgZone,
  OnDestroy, Output,
  PLATFORM_ID,
  QueryList,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { isPlatformServer } from '@angular/common';
import { BreakpointObserver } from '@angular/cdk/layout';
import { combineLatest, interval, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, takeWhile } from 'rxjs/operators';
import { FastDomService } from '@silmar/ng-core/fastdom';
import { NgCarouselItemDirective } from './ng-carousel-item.directive';

/**
 * Visible items configuration interface
 */
export interface VisibleItems {
  sm?: number;
  md?: number;
  lg?: number;
  all: number;
}

@Component({
  selector    : 'si-ng-carousel',
  templateUrl : './ng-carousel.component.html',
  styleUrls   : [ './ng-carousel.component.scss' ]
})
export class NgCarouselComponent implements AfterViewInit, OnDestroy {
  /**
   * The main carousel element
   */
  @ViewChild('carousel', { static : true }) carousel: ElementRef;

  /**
   * The "filmstrip" of the carousel (the wrapper of the items)
   */
  @ViewChild('filmstrip', { static : true }) filmstrip: ElementRef;

  /**
   * Carousel items
   */
  @ContentChildren(NgCarouselItemDirective) items: QueryList<NgCarouselItemDirective>;

  /**
   * Show prev and next buttons
   */
  @Input() showButtons = true;

  /**
   * Loop the carousel once you hit last/first item
   */
  @Input() loop = true;

  /**
   * Auto slide the carousel (ms)
   */
  @Input() autoPlay = 0;

  /**
   * Do not snap to the closest item
   */
  @Input() freeDrag = false;

  /**
   * Give an "elastic feeling" when the user is dragging beyond the bounds of the filmstrip
   */
  @Input() elasticBounds = true;

  @Input() cssDragging = 'dragging';

  @Input() cssStart = 'at-start';

  @Input() cssEnd = 'at-end';

  /**
   * Current item has changed
   */
  @Output() change: EventEmitter<NgCarouselItemDirective[]> = new EventEmitter();

  /**
   * Breakpoints to watch for
   */
  breaks = {
    sm : '(max-width: 959px)',
    md : '(min-width: 960px) and (max-width: 1279px)',
    lg : '(min-width: 1280px)',
    p  : '(orientation:portrait)',
    l  : '(orientation:landscape)'
  };

  /**
   * currently dragging
   */
  dragging = false;

  /**
   * Maximum drag offset (minus)
   */
  maxDrag: number | false = false;

  /**
   * Pan event subject
   */
  drag$: Subject<any> = new Subject<any>();

  /**
   * Known X offsets
   * - last: last known
   * - Temp (for the current dragging session) last known
   */
  protected x = { last : 0, tmp : 0 };

  /**
   * Subscription for the PAN event
   */
  protected dragSb$: Subscription;

  /**
   * Item width subscription
   */
  protected wSb$: Subscription;

  /**
   * Items change subscription
   */
  protected items$: Subscription;

  /**
   * Number of visible items
   */
  protected visConf: VisibleItems = { all : 0 };

  /**
   * Number of visible items
   */
  protected visItems = 0;

  /**
   * Carousel was inited
   */
  protected inited = false;

  /**
   * SSR
   */
  protected isServer = false;

  /**
   * Last state of the observed breakpoints
   */
  protected lastBr: { sm: boolean, md: boolean, lg: boolean, p: boolean } = {
    sm : false,
    md : false,
    lg : true,
    p  : false
  };

  /**
   * Subscription to the breakpoint observer
   */
  protected brkpnt$: Subscription;

  /**
   * Autoloop timer
   */
  protected loop$: Subscription;

  /**
   * MutationObserver
   */
  protected domObs: MutationObserver;

  /**
   * Scheduled slider move flag
   */
  protected willMove = false;

  constructor(@Inject(PLATFORM_ID) platformId, protected rend: Renderer2, protected zone: NgZone, protected dom: FastDomService, breakObs: BreakpointObserver) {
    this.isServer = isPlatformServer(platformId);

    if (!this.isServer) {
      this.brkpnt$ = breakObs.observe([
        this.breaks.sm,
        this.breaks.md,
        this.breaks.lg,
        this.breaks.l,
        this.breaks.p
      ]).subscribe((_) => {
        const state = {
          sm : breakObs.isMatched(this.breaks.sm),
          md : breakObs.isMatched(this.breaks.md),
          lg : breakObs.isMatched(this.breaks.lg),
          p  : breakObs.isMatched(this.breaks.p),
        };

        if (state.sm != this.lastBr.sm || state.md != this.lastBr.md || state.lg != this.lastBr.lg || state.p != this.lastBr.p) {
          this.lastBr = state;
          this.setVisible();
          this.inited && this.init();
        }
      });
    }
  }

  /**
   *
   */
  ngAfterViewInit() {
    if (!this.isServer) {
      this.items$ = this.items.changes.subscribe(() => {
        // force max drag re-subscription && and re-init the whole carousel
        if (this.wSb$) {
          this.wSb$.unsubscribe();
          this.wSb$ = undefined;
        }

        this.init();
      });

      this.dom.mutate(() => this.init().observeDOM());
    }
  }

  /**
   *
   */
  ngOnDestroy() {
    this.dragSb$ && this.dragSb$.unsubscribe();
    this.items$ && this.items$.unsubscribe();
    this.brkpnt$ && this.brkpnt$.unsubscribe();
    this.loop$ && this.loop$.unsubscribe();
    this.wSb$ && this.wSb$.unsubscribe();

    this.delObserver();

    this.autoPlay = 0;
  }

  /**
   * Remove the DOM observer if any
   */
  delObserver() {
    if (this.domObs) {
      this.domObs.disconnect();
      this.domObs = undefined;
    }
  }

  /**
   * Observe the DOM for changes
   */
  observeDOM() {
    this.delObserver();

    /**
     * If we support MutationObserver and we do not have a count of visible items we should observe for dom mutations
     * in order to calculate the correct width of the carousel items
     */
    if (typeof MutationObserver !== 'undefined' && !this.visItems) {
      this.zone.runOutsideAngular(() => {
        this.domObs = new MutationObserver(() => this.zone.run(() => this.init()));

        // TODO Only observe for subtree/childlist changes and reinit then | src and srcset will lead to onload event and width$ will fire for each item
        this.domObs.observe(this.filmstrip.nativeElement, {
          attributes      : true,
          childList       : true,
          subtree         : true,
          attributeFilter : [ 'src', 'srcset' ]
        });
      });
    }
  }

  /**
   * Set number of visible items (set 0 for auto)
   * can be number or VisibleItems type `{ sm?: number, md?: number, lg?: number, all: number }`
   */
  @Input() set visible(items: VisibleItems | number) {
    if (typeof items === 'number') {
      items = { all : items };
    }

    this.visConf = items;

    this.setVisible();
    this.inited && this.init();
  }

  /**
   * Stop the loop on hover
   */
  @HostListener('mouseenter') onHover() {
    this.stopLoop();
  }

  /**
   * Start the loop on mouse out
   */
  @HostListener('mouseleave') onHoverStop() {
    this.startLoop();
  }

  /**
   * Init the carousel
   */
  init() {
    if (!this.isServer) {
      this.inited = true;

      this.reset()
        .setItemsStyle()
        .calcMaxDrag()
        .markCurrent(() => this.change.emit(this.getCurrentItems()))
        .setStartEndCss();

      this.dragSb$ && this.dragSb$.unsubscribe();
      this.loop$ && this.loop$.unsubscribe();

      this.zone.runOutsideAngular(() => {
        this.dragSb$ = this.drag$.pipe(
          filter((_) => this.maxDrag !== 0)
        ).subscribe((ev) => this.drag(ev));
      });

      this.startLoop();

      return this;
    }
  }

  /**
   * Calculate the maximum possible drag offset
   *
   * 0: it cannot be dragged at all
   * false: we do not know yet so drag at will
   *
   */
  calcMaxDrag() {
    this.maxDrag = false;

    if (this.items && this.items.length && !this.wSb$) {
      this.wSb$ && this.wSb$.unsubscribe();

      /**
       * Every carousel item has an observable called width$ that emits a value
       * every time the `width` property gets called or the `calcWidth` method of the carousel item
       *
       * We will listen for width values of all carousel items but will throttle the value editions every 16ms and we
       * will filter any duplicate value emits
       */
      this.wSb$ = combineLatest(this.items.map((item) => {
        item.width;
        return item.width$;
      }))
        .pipe(
          debounceTime(16),
          distinctUntilChanged((a: number[], b: number[]) => this.maxDrag !== false && a.reduce((p, n) => p + n, 0) === b.reduce((p, n) => p + n, 0))
        )
        .subscribe((a) => {
          const w = a.reduce((prev, now) => prev + now, 0);

          if (w) {
            const width  = this.carousel.nativeElement.clientWidth;
            this.maxDrag = w <= width ? 0 : Math.round(Math.abs(((w / width) - 1) * width));
          }
        });
    } else if (this.items && !this.items.length) {
      this.maxDrag = 0;
    }

    return this;
  }

  /**
   * Handle short pan sessions (swipe on left and right) and try to flip to the next/prev item
   */
  onSwipe(ev, dir: 'l' | 'r' = 'l') {
    if (this.maxDrag !== 0 && this.carousel.nativeElement && (this.carousel.nativeElement.clientWidth / 2) > ev.distance) {
      ev.deltaX += this.carousel.nativeElement.clientWidth / 2 * (dir === 'r' ? 1 : -1);
      ev.isFinal = true;

      this.drag(ev);
    }
  }

  /**
   * Go to next slide (works only when visItems is more than 0)
   */
  next(ev?) {
    if (ev) {
      ev.preventDefault();
      ev.stopPropagation();
    }

    if (this.visItems) {
      if (this.isAtEnd()) {
        this.loop && this.moveStrip(this.x.last = 0);
      } else {
        this.moveStrip(this.x.last += this.carousel.nativeElement.clientWidth / this.visItems * -1);
      }

      // try to pin to the nearest item if not freeDrag
      this.moveWithinBounds().markCurrent(() => this.change.emit(this.getCurrentItems())).setStartEndCss();
    }
  }

  /**
   * Go to prev slide (works only when visItems is more than 0)
   */
  prev(ev?) {
    if (ev) {
      ev.preventDefault();
      ev.stopPropagation();
    }

    if (this.visItems) {
      if (this.isAtStart()) {
        this.loop && this.maxDrag !== false && this.moveStrip(this.x.last = (this.maxDrag * -1));
      } else {
        this.moveStrip(this.x.last += this.carousel.nativeElement.clientWidth / this.visItems);
      }

      // try to pin to the nearest item if not freeDrag
      this.moveWithinBounds().markCurrent(() => this.change.emit(this.getCurrentItems())).setStartEndCss();
    }
  }

  select(itemIndex: number) {
    if (! this.items.get(itemIndex)) {
      return false;
    }

    let pos = 0;

    for (let i = 0; i <= itemIndex; i++) {
      if (i == itemIndex) {
        this.moveStrip(this.x.last = pos * -1)
          .moveWithinBounds()
          .markCurrent(() => this.change.emit(this.getCurrentItems()))
          .setStartEndCss();

        return true;
      }

      pos += this.items.get(itemIndex).width;
    }

    return false;
  }

  /**
   * Start the auto loop
   */
  startLoop() {
    this.stopLoop();

    if (this.autoPlay) {
      this.zone.runOutsideAngular(() => {
        this.loop$ = interval(this.autoPlay).pipe(
          takeWhile(() => this.autoPlay > 0)
        ).subscribe(() => this.next());
      });
    }
  }

  /**
   * Stop the autoloop
   */
  stopLoop() {
    this.loop$ && this.loop$.unsubscribe();
  }

  /**
   * We are at the first slide item
   */
  isAtStart() {
    return this.x.last >= 0;
  }

  /**
   * We are at the last slide item
   */
  isAtEnd() {
    return this.maxDrag !== false && Math.abs(this.x.last) >= this.maxDrag;
  }

  /**
   * Get array of current items
   */
  getCurrentItems(): NgCarouselItemDirective[] {
    return this.items.filter((item) => item.isCurrent);
  }

  /**
   * We are being dragged
   */
  protected drag(ev) {
    // leftover pan events
    if (!this.dragging && ev.isFinal) {
      return;
    }

    /**
     * If !dragging then this is the start of a new drag
     */
    if (!this.dragging) {
      this.setDragging();

      this.x.tmp = 0;
    }

    /**
     * add the last known position to the deltaX
     */
    let newPos = ev.deltaX + this.x.last;

    /**
     * Make adjustments:
     * If the new position is more than 0 we can't really drag more in that direction
     * If we have maxDrag we cannot drag more than that
     * this.elasticBounds gives a bit more pleasant experience
     */
    if (newPos > 0) {
      newPos = this.elasticBounds ? newPos * 0.1 : 0;
    } else if (this.maxDrag !== false && Math.abs(newPos) > this.maxDrag) {
      const tmp = this.maxDrag * Math.sign(newPos);
      newPos    = tmp + (this.elasticBounds ? (newPos - tmp) * 0.1 : 0);
    }

    /**
     * The last drag event has isFinal to true
     */
    if (ev.isFinal) {
      this.x.last = newPos;

      this.delDragging();

      // give it some time and then try to pin to the nearest item if not freeDrag
      setTimeout(() => this.pinPosition());
    }

    /**
     * Move the strip if we have new position
     */
    if (this.x.tmp !== newPos) {
      this.x.tmp = newPos;
      this.moveStrip(newPos ? newPos : null);
    }
  }

  /**
   * If we cannot free drag we should get to the nearest item after the drag was finished
   */
  protected pinPosition() {
    this.moveWithinBounds();

    const pos = Math.abs(this.x.last);

    if (!this.freeDrag && this.x.last !== 0 && pos !== this.maxDrag) {
      let newPos = 0;

      /**
       * Get the position of the left most item that has it's half width within view
       */
      for (const item of this.items) {
        const w = item.width;

        if (newPos + (w / 2) >= pos) {
          continue;
        }

        newPos += w;
      }

      this.x.last = newPos * Math.sign(this.x.last);

      /**
       * Don't go beyond maxDrag if set (this will effectively disregard the most left position of the item)
       */
      if (this.maxDrag && newPos > this.maxDrag) {
        this.x.last = this.maxDrag * Math.sign(this.x.last);
      }

      this.moveStrip(this.x.last);
    }

    this.markCurrent(() => this.change.emit(this.getCurrentItems())).setStartEndCss();
  }

  /**
   * Mark carousel items that are current
   */
  protected markCurrent(clb: () => void = null) {
    this.dom.measure(() => {
      const posMin = Math.abs(this.x.last);
      const posMax = posMin + this.carousel.nativeElement.clientWidth;
      let tmpPos   = 0;

      this.items.map((item) => {
        item.current = tmpPos >= posMin && tmpPos < posMax;

        tmpPos += item.width;

        if (!item.isCurrent) {
          item.current = tmpPos > posMin && tmpPos < posMax;
        }
      });

      clb && this.zone.run(() => clb());
    });

    return this;
  }

  /**
   * Set end/start css classes
   */
  protected setStartEndCss() {
    const end   = this.isAtEnd();
    const start = this.isAtStart();

    this.dom.mutate(() => {
      if (end) {
        this.rend.addClass(this.carousel.nativeElement, this.cssEnd);
      } else {
        this.rend.removeClass(this.carousel.nativeElement, this.cssEnd);
      }

      if (start) {
        this.rend.addClass(this.carousel.nativeElement, this.cssStart);
      } else {
        this.rend.removeClass(this.carousel.nativeElement, this.cssStart);
      }
    });

    return this;
  }

  /**
   * If the filmstrip is out of bounds (0 and maxDrag) move it back within
   */
  protected moveWithinBounds() {
    if (this.x.last > 0) {
      this.moveStrip(this.x.last = 0);
    } else if (this.maxDrag !== false && Math.abs(this.x.last) > this.maxDrag) {
      this.moveStrip(this.x.last = this.maxDrag * Math.sign(this.x.last));
    }

    return this;
  }

  /**
   * Set the flex styles to the carousel items
   */
  protected setItemsStyle() {
    const flex = this.visItems != 0 ? (100 / this.visItems) + '%' : 'auto';

    this.items.map((item) => item.setFlex(flex));

    return this;
  }

  /**
   * Reset the carousel
   */
  protected reset() {
    this.x.last   = 0;
    this.maxDrag  = false;
    this.dragging = false;
    this.items.map((item, index) => item.reset(index));
    this.moveStrip(0).delDragging();

    return this;
  }

  /**
   * Move the film strip with css transform
   */
  protected moveStrip(pos: number) {
    if (this.willMove) {
      this.dom.clear(this.willMove);
    }

    this.willMove = this.dom.mutate(() => {
      this.willMove = this.dom.clear(this.willMove);
      this.rend.setStyle(this.filmstrip.nativeElement, 'transform', pos ? 'translateX(' + pos + 'px)' : '');
    });

    return this;
  }

  /**
   * Add dragging classes
   */
  protected setDragging() {
    this.dragging = true;

    this.dom.mutate(() => {
      this.rend.addClass(this.filmstrip.nativeElement, this.cssDragging);
      this.rend.addClass(this.carousel.nativeElement, this.cssDragging);
    });

    return this;
  }

  /**
   * Remove dragging classes
   */
  protected delDragging() {
    this.dragging = false;

    this.dom.mutate(() => {
      this.rend.removeClass(this.filmstrip.nativeElement, this.cssDragging);
      this.rend.removeClass(this.carousel.nativeElement, this.cssDragging);
    });

    return this;
  }

  /**
   *
   */
  protected setVisible() {
    if (this.lastBr.sm) {
      this.visItems = this.visConf.sm !== undefined ? this.visConf.sm : (this.visConf.all || 0);
    } else if (this.lastBr.md) {
      this.visItems = this.visConf.md !== undefined ? this.visConf.md : (this.visConf.all || 0);
    } else if (this.lastBr.lg) {
      this.visItems = this.visConf.lg !== undefined ? this.visConf.lg : (this.visConf.all || 0);
    } else {
      this.visItems = this.visConf.all || 0;
    }
  }
}

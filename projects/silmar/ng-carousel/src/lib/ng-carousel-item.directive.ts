import { AfterViewInit, Directive, ElementRef, Inject, OnDestroy, PLATFORM_ID, Renderer2 } from '@angular/core';
import { Subject } from 'rxjs';
import { FastDomService } from '@silmar/ng-core/fastdom';
import { isPlatformServer } from '@angular/common';

@Directive({
  selector : '[siCarouselItem]',
  exportAs : 'siCarouselItem'
})
export class NgCarouselItemDirective implements OnDestroy, AfterViewInit {
  width$: Subject<number> = new Subject();

  index = 0;

  protected widthPr: number;

  protected isServer: boolean;

  private IS_CURRENT = false;

  constructor(@Inject(PLATFORM_ID) platformId, protected el: ElementRef, protected rend: Renderer2, protected dom: FastDomService) {
    this.isServer = isPlatformServer(platformId);
  }

  /**
   * Get the native DOM element
   */
  getNative() {
    return this.el.nativeElement;
  }

  ngAfterViewInit(): void {
    if (!this.isServer) {
      if (this.el.nativeElement.tagName === 'IMG') {
        this.el.nativeElement.onload = () => this.onImgLoad();
      } else {
        const imgs = this.el.nativeElement.querySelectorAll('img');

        for (const img of imgs) {
          img.onload = () => this.onImgLoad();
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.width$.complete();
  }

  /**
   * Get the cached width of the item / this might be zero but it will trigger width measure if it is
   */
  get width() {
    if (!this.widthPr) {
      this.calcWidth();
    }

    return this.widthPr;
  }

  /**
   * Set the item flex percent (or 'auto')
   * This will recalculate the item width
   */
  setFlex(percent) {
    this.dom.mutate(() => this.rend.setStyle(this.el.nativeElement, 'flex', '0 0 ' + percent));
    this.calcWidth();
  }

  /**
   * Set if the item is currently visible
   */
  set current(isCurrent: boolean) {
    this.IS_CURRENT = isCurrent;

    if (isCurrent) {
      this.dom.mutate(() => this.rend.addClass(this.el.nativeElement, 'is-current'));
    } else {
      this.dom.mutate(() => this.rend.removeClass(this.el.nativeElement, 'is-current'));
    }
  }

  /**
   * Check if the item is one of the current items
   */
  get isCurrent() {
    return this.IS_CURRENT;
  }

  /**
   * Reset the item
   */
  reset(index: number) {
    this.index   = index;
    this.widthPr = undefined;

    return this;
  }

  /**
   * Calculate the item width (this will get into account the margins of the box)
   */
  calcWidth() {
    !this.isServer && this.dom.measure(() => {
      const box    = this.el.nativeElement.getBoundingClientRect();
      const styles = getComputedStyle(this.el.nativeElement);
      this.widthPr = box.width ? box.width + parseInt(styles.marginLeft) + parseInt(styles.marginRight) : 0;

      this.width$.next(this.widthPr);
    });

    return this;
  }

  /**
   * Reset the item on image (if any) load
   */
  protected onImgLoad() {
    this.reset(this.index).calcWidth();
  }
}

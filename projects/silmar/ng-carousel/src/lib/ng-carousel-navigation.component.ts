import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, ElementRef, Inject,
  Input,
  OnDestroy, PLATFORM_ID
} from '@angular/core';
import { NgCarouselComponent } from './ng-carousel.component';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { FastDomService } from '@silmar/ng-core/fastdom';
import { isPlatformServer } from '@angular/common';

@Component({
  selector        : 'si-ng-carousel-navigation',
  template        : `
    <ng-container *ngFor="let item of previewObjs; let i = index">
      <img *ngIf="item.src" [attr.src]="item.src" (click)="carousel.select(i)" alt=""
           [class.current]="item.current" class="nav-item"/>
    </ng-container>
  `,
  styles          : [ `
    :host {
      --current-color: #7253ed;
      --border-width: 2px;
      --item-spacing: 1rem;
      --flex-flow: row;

      display: flex;
      position: relative;
      flex-flow: var(--flex-flow);
      overflow: auto;
      scroll-snap-type: both proximity;
      scroll-behavior: smooth;
    }

    :host > .nav-item {
      scroll-snap-align: start;
    }

    .nav-item {
      cursor: pointer;
      max-height: 60px;
      transition-property: border-bottom-color, border-left-color, border-right-color, border-top-color;
      transition: 100ms;

      margin-right: var(--item-spacing);
      border: var(--border-width) solid transparent;
    }

    .nav-item.current {
      border: var(--border-width) solid var(--current-color);
    }
  ` ],
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class NgCarouselNavigationComponent implements AfterViewInit, OnDestroy {
  @Input() carousel: NgCarouselComponent;

  @Input() previews: SafeResourceUrl[] | string[];

  previewObjs: { src: SafeResourceUrl, current: boolean }[] = [];

  protected subscr$: Subscription;

  protected isServer: boolean;

  constructor(@Inject(PLATFORM_ID) platformId, protected cd: ChangeDetectorRef, protected elRef: ElementRef, protected dom: FastDomService) {
    this.isServer = isPlatformServer(platformId);
  }

  ngOnDestroy(): void {
    this.subscr$ && this.subscr$.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.init();
  }

  protected init() {
    if (!this.carousel || !this.previews || this.isServer) {
      return false;
    }

    this.setPreviews();

    this.subscr$ = this.carousel.change
      .asObservable()
      .subscribe(
        items => {
          this.previewObjs.map(item => item.current = false);

          const elements = this.elRef.nativeElement.querySelectorAll('img.nav-item');

          for (const item of items) {
            this.previewObjs[ item.index ] && (this.previewObjs[ item.index ].current = true);

            if (elements[ item.index ]) {
              this.dom
                .mutate(() => this.elRef.nativeElement.scroll(elements[ item.index ].offsetLeft, elements[ item.index ].offsetTop));
            }
          }

          this.cd.markForCheck();
        }
      );
  }

  protected setPreviews() {
    this.previewObjs = [];

    for (const item of this.previews) {
      this.previewObjs.push({ src : item, current : false });
    }

    setTimeout(_ => this.cd.markForCheck());
  }
}

# @silmar/ng-carousel

[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-carousel.svg)](https://www.npmjs.com/package/@silmar/ng-carousel)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-carousel/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-ng-carousel/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-carousel.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-carousel)

Really simple carousel

### Install
```
npm i @silmar/ng-carousel hammerjs
// or
yarn add @silmar/ng-carousel hammerjs
```

### Usage
The very basic usage is as follows:

`main.ts`
```typescript
// ....
import 'hammerjs';
// ....
```

`app.module.ts`
```typescript
import {BrowserModule} from '@angular/platform-browser';
import {NgModule, HammerModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgCarouselModule} from '@silmar/ng-carousel';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HammerModule, // <-- For Angular 9
        NgCarouselModule // <-- import the carousel module
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
```
`app.component.html`
```angular2html
<!-- this will create a carousel with 2 visible items per slide -->
<si-ng-carousel [visible]="2">
  <div *ngFor="let img of gallery" siCarouselItem>
    <img [src]="domS.bypassSecurityTrustResourceUrl(img)" style="height: 400px"/>
  </div>
</si-ng-carousel>
```

### Demo
Visit the [demo](https://etg-public.gitlab.io/silmar-ng-carousel/)

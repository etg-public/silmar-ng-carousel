# NG Carousel

[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-carousel.svg)](https://www.npmjs.com/package/@silmar/ng-carousel)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-carousel/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-ng-carousel/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-carousel.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-carousel)

### Install
```
npm i @silmar/ng-carousel hammerjs
// or
yarn add @silmar/ng-carousel hammerjs
```

### Demo
[Checkout the demo page](https://etg-public.gitlab.io/silmar-ng-carousel) or run the demo app, check out the project then run the following in th root directory

```
npm i
ng serve
```

More info in the library [README.md](projects/silmar/ng-carousel/README.md)


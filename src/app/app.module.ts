import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig, HammerModule } from '@angular/platform-browser';
import { Injectable, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgCarouselModule } from '../../projects/silmar/ng-carousel/src/lib/ng-carousel.module';

@Injectable()
export class CustomHammerConfig extends HammerGestureConfig {
  overrides = {
    pinch  : { enable : false },
    rotate : { enable : false }
  } as any;
}


@NgModule({
  declarations : [
    AppComponent
  ],
  imports      : [
    BrowserModule,
    NgCarouselModule,
    HammerModule
  ],
  providers    : [
    { provide : HAMMER_GESTURE_CONFIG, useClass : CustomHammerConfig }
  ],
  bootstrap    : [ AppComponent ]
})
export class AppModule {
}

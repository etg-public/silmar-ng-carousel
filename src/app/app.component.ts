import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector    : 'app-root',
  templateUrl : './app.component.html',
  styleUrls   : [ './app.component.scss' ]
})
export class AppComponent {
  gallery: any[] = [
    'https://images.unsplash.com/photo-1509904785290-42ab09175ef0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=80',
    'https://static1.squarespace.com/static/593ac92e6b8f5b0bb5b03648/t/59dae34bedaed846531a2a8f/1507517273573/sugar-glider-1-2.jpg?format=750w',
    'https://i.pinimg.com/originals/51/8d/f0/518df057f801845928d688fe9f6e0786.jpg',
    'https://farm8.staticflickr.com/7066/6891416813_b9244742b1_b.jpg',
    'https://upload.wikimedia.org/wikipedia/commons/5/5b/Petaurus_Breviceps_Petauro_dello_Zucchero_2.jpg',
    'https://images.unsplash.com/photo-1536248337121-aebeba487bd4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=80',
    'https://upload.wikimedia.org/wikipedia/commons/9/97/Sugar_Gliders_eating_Mealworms.jpg'
  ];
  gallery2       = [];
  gallery3       = [];
  txt            = {
    css     : `img {
    pointer-events: none;
}`,
    install : 'yarn add @silmar/ng-carousel hammerjs',
    module  : `import {NgCarouselModule} from '@silmar/ng-carousel';
// ...
@NgModule({
    declarations: [ AppComponent ],
    imports: [
        //...
        HammerModule, // <-- For Angular 9
        NgCarouselModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}`,
    ex1     : `<si-ng-carousel [visible]="2">
  <div *ngFor="let img of gallery" siCarouselItem class="item">
    <img [src]="img"/>
  </div>
</si-ng-carousel>`,
    ex2     : `<si-ng-carousel [visible]="{sm:1,all:2}">
  <div *ngFor="let img of gallery" siCarouselItem class="item">
    <img [src]="img"/>
  </div>
</si-ng-carousel>`,
    ex3     : `<si-ng-carousel [visible]="0" [showButtons]="false" [freeDrag]="true">
    <div *ngFor="let img of gallery" siCarouselItem class="item-auto">
        <img [src]="img"/>
    </div>
</si-ng-carousel>`,
    ex4: `<si-ng-carousel [visible]="1" #carousel>
    <div *ngFor="let img of gallery3" siCarouselItem class="item"><img [src]="img"/></div>
</si-ng-carousel>
<h5>Standard Navigation</h5>
<si-ng-carousel-navigation [carousel]="carousel" [previews]="gallery3"></si-ng-carousel-navigation>

<h5>Customized Navigation with CSS variables</h5>
<si-ng-carousel-navigation [carousel]="carousel" [previews]="gallery3"
                           style="--current-color: purple;--border-width: 5px;--item-spacing: .1rem"></si-ng-carousel-navigation>
`
  };

  constructor(public domS: DomSanitizer) {
    this.gallery  = this.gallery.map((item) => domS.bypassSecurityTrustResourceUrl(item));
    this.gallery2 = [ ...this.gallery ].sort(() => .5 - Math.random());
    this.gallery3 = [ ...this.gallery ].sort(() => .5 - Math.random());
  }

}
